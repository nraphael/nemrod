<?php

/*
 * This file is part of the Nemrod package.
 *
 * (c) Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Conjecto\Nemrod\Bundle\NemrodBundle\DependencyInjection\CompilerPass;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use EzSystems\EzPlatformPageFieldType\FieldType\Page\Service\BlockService;
use EzSystems\EzPlatformPageBuilderBundle\Controller\PreviewController;
use Symfony\Component\DependencyInjection\Reference;

class EventListenerRegistrationPass implements CompilerPassInterface
{
    /**
     * You can modify the container here before it is dumped to PHP code.
     *
     * @param ContainerBuilder $container
     *
     * @api
     */
    public function process(ContainerBuilder $container)
    {

        if (!$container->hasDefinition('debug.event_dispatcher.inner')) {
            $container->setAlias('debug.event_dispatcher.inner', 'event_dispatcher');
        }

        $BlockServiceDefinition = $container->findDefinition('EzSystems\EzPlatformPageFieldType\FieldType\Page\Service\BlockService');

        $eventDispatcher = new Reference('debug.event_dispatcher.inner');
        $rendererInterface = new Reference('EzSystems\EzPlatformPageFieldType\FieldType\Page\Block\Renderer\RendererInterface');
        $blockDefinitionFactory = new Reference('EzSystems\EzPlatformPageFieldType\FieldType\Page\Block\Definition\BlockDefinitionFactory');

        $argument = $BlockServiceDefinition->setArgument('$eventDispatcher', $eventDispatcher);
        $argument = $BlockServiceDefinition->setArgument('$renderer', $rendererInterface);
        $argument = $BlockServiceDefinition->setArgument('$blockDefinitionFactory', $blockDefinitionFactory);

        $BlockServiceDefinition->setAutowired(false);

        $PreviewControllerDefinition = $container->findDefinition('EzSystems\EzPlatformPageBuilderBundle\Controller\PreviewController');

        $layoutDefinitionRegistry = new Reference('EzSystems\EzPlatformPageFieldType\Registry\LayoutDefinitionRegistry');
        $configResolver = new Reference('eZ\Publish\Core\MVC\ConfigResolverInterface');
        $blockService = new Reference('EzSystems\EzPlatformPageFieldType\FieldType\Page\Service\BlockService');
        $pageFieldType = new Reference('EzSystems\EzPlatformPageFieldType\FieldType\LandingPage\Type');
        $httpKernel = new Reference('Symfony\Component\HttpKernel\HttpKernelInterface');
        $previewHelper = new Reference('ezpublish.content_preview_helper');

        $argument = $PreviewControllerDefinition->setArgument('$layoutDefinitionRegistry', $layoutDefinitionRegistry);
        $argument = $PreviewControllerDefinition->setArgument('$configResolver', $configResolver);
        $argument = $PreviewControllerDefinition->setArgument('$blockService', $blockService);
        $argument = $PreviewControllerDefinition->setArgument('$pageFieldType', $pageFieldType);
        $argument = $PreviewControllerDefinition->setArgument('$httpKernel', $httpKernel);
        $argument = $PreviewControllerDefinition->setArgument('$previewHelper', $previewHelper);
        $argument = $PreviewControllerDefinition->setArgument('$defaultBaseTemplate', '%ezpublish.content_view.viewbase_layout%');
        $argument = $PreviewControllerDefinition->setArgument('$eventDispatcher', $eventDispatcher);

        $PreviewControllerDefinition->setAutowired(false);

        $tmpDispatchers = $container->findTaggedServiceIds('nemrod.event_dispatcher');
        if (!$tmpDispatchers) {
            return;
        }

        $dispatchers = array();

        foreach ($tmpDispatchers as $key => $dispatcherTags) {
            foreach ($dispatcherTags as $dispatcherTag) {
                $endP = (isset($dispatcherTag['endpoint'])) ? $dispatcherTag['endpoint'] : 'default';
                $dispatchers[$endP] = $key;
            }
        }

        //finding and registering listeners
        $listeners = $container->findTaggedServiceIds('nemrod.resource_event_listener');

        if (!empty($listeners)) {
            foreach ($dispatchers as $endPoint => $dispatcher) {
                foreach ($listeners as $listId => $listenerTags) {
                    $listenerDef = $container->getDefinition($listId);
                    foreach ($listenerTags as $tag) {
                        if (isset($tag['endpoint']) &&
                            isset($dispatchers[$tag['endpoint']]) &&
                            ($dispatchers[$tag['endpoint']] === $dispatcher)) {
                            $def = $container->getDefinition($dispatchers[$tag['endpoint']]);
                            $def->addMethodCall('addListener', array($tag['event'], array($listenerDef, $tag['method'])));
                        } elseif (!isset($tag['endpoint'])) {
                            // if no endpoint is defined for listener, it is registered to all
                            // dispatchers
                            $def = $container->getDefinition($dispatcher);
                            $def->addMethodCall('addListener', array($tag['event'], array($listenerDef, $tag['method'])));
                        }
                    }
                }
            }
        }

        //finding and registering subscribers
        $subscribers = $container->findTaggedServiceIds('nemrod.resource_event_subscriber');
        if (!empty($subscribers)) {
            foreach ($dispatchers as $endPoint => $dispatcher) {
                foreach ($subscribers as $listId => $listenerTags) {
                    $listenerDef = $container->getDefinition($listId);
                    foreach ($listenerTags as $tag) {
                        if (isset($tag['endpoint']) &&
                            isset($dispatchers[$tag['endpoint']]) &&
                            ($dispatchers[$tag['endpoint']] === $dispatcher)) {
                            $def = $container->getDefinition($dispatchers[$tag['endpoint']]);
                            $def->addMethodCall('addsubscriber', array($listenerDef));
                        } elseif (!isset($tag['endpoint'])) {
                            // if no endpoint is defined for listener, it is registered to all
                            // dispatchers
                            $def = $container->getDefinition($dispatcher);
                            $def->addMethodCall('addSubscriber', array($listenerDef));
                        }
                    }
                }
            }
        }
    }
}
